FROM node:18.10.0-buster AS builder

# Fix for JS heap limit allocation issue
ENV NODE_OPTIONS="--max-old-space-size=4096"

RUN mkdir -p /app

WORKDIR /app

ADD ./package.json ./package.json
ADD ./yarn.lock ./yarn.lock

RUN yarn

COPY . .

RUN yarn global add @nestjs/cli 

RUN yarn build

RUN yarn install --production --ignore-scripts --prefer-offline

FROM debian:11

RUN apt-get update -yq \
    && apt-get install curl gnupg zip -yq \
    && apt-get install -yq build-essential \
    && apt-get clean -y

RUN curl -O https://nodejs.org/dist/v18.3.0/node-v18.3.0-linux-x64.tar.xz \
    && tar -xf node-v18.3.0-linux-x64.tar.xz \
    && mv node-v18.3.0-linux-x64 /usr/local/lib/nodejs \
    && echo 'export PATH="/usr/local/lib/nodejs/bin:$PATH"' >> /etc/profile.d/nodejs.sh \
    && /bin/bash -c "source /etc/profile.d/nodejs.sh" \
    && rm node-v18.3.0-linux-x64.tar.xz
ENV PATH=/usr/local/lib/nodejs/bin:$PATH

ENV NODE_ENV=production
ENV NODE_OPTIONS="--max-old-space-size=4096"

RUN apt-get update && \
    apt-get install -y libaio1 wget && \
    apt-get -o Dpkg::Options::="--force-confold" upgrade -q -y --force-yes && \
    apt-get -y autoremove && \
    apt-get -y autoclean

# Install Instantclient Basic Light Oracle and Dependencies
WORKDIR /opt/oracle

RUN wget https://download.oracle.com/otn_software/linux/instantclient/instantclient-basiclite-linuxx64.zip && \
    unzip instantclient-basiclite-linuxx64.zip && rm -f instantclient-basiclite-linuxx64.zip && \
    cd /opt/oracle/instantclient* && rm -f *jdbc* *occi* *mysql* *mql1* *ipc1* *jar uidrvci genezi adrci && \
    echo /opt/oracle/instantclient* > /etc/ld.so.conf.d/oracle-instantclient.conf && ldconfig
WORKDIR /

RUN mkdir -p /app

COPY --from=builder /app/package.json ./app/package.json
COPY --from=builder /app/node_modules ./app/node_modules
COPY --from=builder /app/dist ./app/dist

RUN npm install -g npm@9.6.6

# Define non-sudo user
RUN useradd --create-home appuser \
    && chown -R appuser:appuser /app
USER appuser

WORKDIR /app
