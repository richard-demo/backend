import { NestFactory } from '@nestjs/core';
import {
  HttpStatus,
  UnprocessableEntityException,
  ValidationPipe,
} from '@nestjs/common';
import { ExpressAdapter } from '@nestjs/platform-express';
import type { NestExpressApplication } from '@nestjs/platform-express';
import rateLimit from 'express-rate-limit';
import helmet from 'helmet';
import { Logger } from 'nestjs-pino';
import compression from 'compression';
import { urlencoded, json } from 'express';
import cookieParser from 'cookie-parser';

import { AllExceptionsFilter } from 'filters/all-exceptions-filter';

import { SharedModule } from 'shared/shared.module';
import { ApiConfigService } from 'shared/services/api-config.service';

import { AppModule } from './app.module';
import { setupSwagger } from './setup-swagger';

async function bootstrap(): Promise<NestExpressApplication> {
  const app = await NestFactory.create<NestExpressApplication>(
    AppModule,
    new ExpressAdapter(),
    { cors: true },
  );
  app.useLogger(app.get(Logger));

  app.useGlobalFilters(new AllExceptionsFilter(app.get(Logger)));

  app.enableCors({
    origin: true,
    credentials: true,
  });

  app.use(helmet());
  app.use(
    rateLimit({
      windowMs: 15 * 60 * 1000, // 15 minutes
      max: 100, // limit each IP to 100 requests per windowMs
    }),
  );

  app.use(compression());
  app.enableVersioning();

  app.setGlobalPrefix('api');

  app.use(cookieParser());
  app.use(json({ limit: '50mb' }));
  app.use(
    urlencoded({ extended: true, limit: '50mb', parameterLimit: 1000000 }),
  );

  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
      errorHttpStatusCode: HttpStatus.UNPROCESSABLE_ENTITY,
      transform: true,
      dismissDefaultMessages: true,
      exceptionFactory: (errors) => new UnprocessableEntityException(errors),
    }),
  );

  const configService = app.select(SharedModule).get(ApiConfigService);

  if (configService.documentationEnabled) {
    setupSwagger(app);
  }

  // Starts listening for shutdown hooks
  if (!configService.isDevelopment) {
    app.enableShutdownHooks();
  }

  const port = configService.appConfig.port;

  await app.listen(port, '0.0.0.0', async function () {
    console.log(`Ready to use at ${await app.getUrl()} 🚀`);
  });

  return app;
}

void bootstrap();
