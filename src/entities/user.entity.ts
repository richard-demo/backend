import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  BeforeInsert,
  BeforeUpdate,
  BaseEntity,
} from 'typeorm';
import bcrypt from 'bcrypt';

export enum USER_STATUS {
  NOT_ACTIVATED = 'not_activated',
  ACTIVATED = 'activated',
  ARCHIVED = 'archived',
}

@Entity({ name: 'users' })
export class UserEntity extends BaseEntity {
  @BeforeInsert()
  @BeforeUpdate()
  hashPassword(): void {
    if (this.password) {
      this.password = bcrypt.hashSync(this.password, 10);
    }
  }

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column({ unique: true })
  email: string;

  @Column({
    type: 'enum',
    enum: USER_STATUS,
    default: USER_STATUS.NOT_ACTIVATED,
  })
  status: USER_STATUS;

  @Column({ name: 'password_digest' })
  password: string;

  async isMatchPassword(password: string): Promise<boolean> {
    return bcrypt.compare(password, this.password);
  }
}
