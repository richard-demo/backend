import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FindOneOptions, Repository } from 'typeorm';

import { USER_STATUS, UserEntity } from 'entities/user.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private usersRepository: Repository<UserEntity>,
  ) {}

  async create(userParams: Partial<UserEntity>): Promise<UserEntity> {
    const { email, firstName, lastName, password, status } = userParams;

    const user = new UserEntity();
    user.email = email;
    user.firstName = firstName;
    user.lastName = lastName;
    user.password = password;
    user.status = status;

    return this.usersRepository.save(user);
  }

  async findByEmail(email: string, status?: USER_STATUS | Array<USER_STATUS>) {
    let query = this.usersRepository
      .createQueryBuilder('users')
      .where('users.email = :email', { email });

    if (status) {
      const statusList = typeof status === 'object' ? status : [status];
      query = query.andWhere('users.status IN (:...statusList)', {
        statusList: statusList,
      });
    }

    return query.getOne();
  }

  findById(id: string): Promise<UserEntity> {
    return this.usersRepository.findOneBy({ id });
  }
}
