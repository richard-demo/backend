import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtModule } from '@nestjs/jwt';

import { UserModule } from 'modules/user/user.module';
import { UserSessionsEntity } from 'entities/user_sessions.entity';

import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { SessionService } from './session.service';
import { JwtStrategy } from './jwt.strategy';

import { ApiConfigService } from 'shared/services/api-config.service';

@Module({
  imports: [
    UserModule,
    TypeOrmModule.forFeature([UserSessionsEntity]),
    JwtModule.registerAsync({
      useFactory: (configService: ApiConfigService) => ({
        privateKey: configService.authConfig.privateKey,
        publicKey: configService.authConfig.publicKey,
        signOptions: {
          algorithm: 'RS256',
          expiresIn: configService.authConfig.jwtExpirationTime,
        },
        verifyOptions: {
          algorithms: ['RS256'],
        },
      }),
      inject: [ApiConfigService],
    }),
  ],
  providers: [AuthService, SessionService, JwtStrategy],
  controllers: [AuthController],
})
export class AuthModule {}
