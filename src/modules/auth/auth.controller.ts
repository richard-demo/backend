import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';

import { SignInDto, SignUpDto } from './dto/auth.dto';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from './jwt-auth.guard';
import { User } from 'decorators/user.decorator';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}
  @Post('/sign-in')
  async signIn(@Body() body: SignInDto) {
    return this.authService.signIn(body.email, body.password);
  }

  @Post('/sign-up')
  async signUp(@Body() body: SignUpDto) {
    return this.authService.signUp(
      body.firstName,
      body.lastName,
      body.email,
      body.password,
    );
  }

  @Get('/validate')
  @UseGuards(JwtAuthGuard)
  async validate(@User() user) {
    return {
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
    };
  }
}
