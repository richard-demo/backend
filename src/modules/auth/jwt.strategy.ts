import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';

import { UserService } from 'modules/user/user.service';
import { ApiConfigService } from 'shared/services/api-config.service';
import { USER_STATUS, UserEntity } from 'entities/user.entity';

export type JWTPayload = {
  sessionId: string;
  sub: string;
};

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private readonly userService: UserService,
    private readonly configService: ApiConfigService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.authConfig.publicKey,
    });
  }

  async validate(payload: JWTPayload) {
    const user: UserEntity = await this.userService.findById(payload.sub);

    if (!user || user.status !== USER_STATUS.ACTIVATED) return false;

    return user;
  }
}
