import {
  Injectable,
  NotAcceptableException,
  UnauthorizedException,
} from '@nestjs/common';
import { Response } from 'express';
import * as requestIp from 'request-ip';
import { JwtService } from '@nestjs/jwt';

import { USER_STATUS, UserEntity } from 'entities/user.entity';
import { UserService } from 'modules/user/user.service';
import { getUserErrorMessages } from 'helpers/user_lifecycle.helper';
import { SessionService } from './session.service';
import { RequestContext } from 'models/request-context.model';
import { JWTPayload } from './jwt.strategy';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private sessionService: SessionService,
    private readonly jwtService: JwtService,
  ) {}

  private async validateUser(
    email: string,
    password: string,
  ): Promise<UserEntity> {
    const user = await this.userService.findByEmail(email);
    if (!user) {
      throw new UnauthorizedException('Invalid credentials');
    }

    if (user.status !== USER_STATUS.ACTIVATED) {
      throw new UnauthorizedException(getUserErrorMessages(user.status));
    }

    if (!(await user.isMatchPassword(password))) {
      throw new UnauthorizedException('Invalid credentials');
    }

    return user;
  }

  private async generateSignInResultPayload(user: UserEntity): Promise<any> {
    const request = RequestContext?.currentContext?.req;

    const session = await this.sessionService.createSession(
      user.id,
      request?.headers['user-agent'] || 'unknown',
      requestIp.getClientIp(request) || 'unknown',
    );

    const jwtPayload: JWTPayload = {
      sessionId: session.id,
      sub: user.id,
    };

    const token = await this.jwtService.signAsync(jwtPayload);

    return {
      id: user.id,
      email: user.email,
      firstName: user.firstName,
      lastName: user.lastName,
      token,
    };
  }

  async signIn(email: string, password: string) {
    const user = await this.validateUser(email, password);

    return this.generateSignInResultPayload(user);
  }

  async signUp(
    firstName: string,
    lastName: string,
    email: string,
    password: string,
  ) {
    const existingUser = await this.userService.findByEmail(email);
    if (existingUser) throw new NotAcceptableException('Email already exists');

    const user = await this.userService.create({
      firstName,
      lastName,
      email,
      password,
      status: USER_STATUS.ACTIVATED,
    });

    return this.generateSignInResultPayload(user);
  }
}
