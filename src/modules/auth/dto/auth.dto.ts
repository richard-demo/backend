import { IsEmail, IsNotEmpty, IsString, MinLength } from 'class-validator';
import { lowercaseString } from 'helpers/utils.helper';
import { Transform } from 'class-transformer';

export class SignInDto {
  @IsEmail()
  @Transform(({ value }) => lowercaseString(value))
  @IsNotEmpty()
  email: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(5, { message: 'Password should contain more than 5 letters' })
  password: string;
}

export class SignUpDto {
  @IsString()
  @Transform(({ value }) => value?.trim())
  @IsNotEmpty()
  firstName: string;

  @IsString()
  @Transform(({ value }) => value?.trim())
  @IsNotEmpty()
  lastName: string;

  @IsEmail()
  @Transform(({ value }) => lowercaseString(value))
  @IsNotEmpty()
  email: string;

  @IsString()
  @IsNotEmpty()
  @MinLength(5, { message: 'Password should contain more than 5 letters' })
  password: string;
}
