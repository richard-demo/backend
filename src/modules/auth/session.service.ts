import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { UserSessionsEntity } from 'entities/user_sessions.entity';
import { ApiConfigService } from 'shared/services/api-config.service';

@Injectable()
export class SessionService {
  constructor(
    @InjectRepository(UserSessionsEntity)
    private userSessionsRepository: Repository<UserSessionsEntity>,
    private configService: ApiConfigService,
  ) {}

  async createSession(
    userId: string,
    device: string,
    ip: string,
  ): Promise<UserSessionsEntity> {
    const session = new UserSessionsEntity();
    session.userId = userId;
    session.device = device;
    session.ip = ip;
    session.createdAt = new Date();
    session.expiry = this.getSessionExpiry();

    return this.userSessionsRepository.save(session);
  }

  private getSessionExpiry(): Date {
    const now = new Date();
    return new Date(
      now.getTime() + this.configService.authConfig.jwtExpirationTime * 1000,
    );
  }
}
