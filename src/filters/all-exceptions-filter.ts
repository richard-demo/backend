import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  HttpException,
  HttpStatus,
} from '@nestjs/common';
import { Logger } from 'nestjs-pino';
import { QueryFailedError } from 'typeorm';

import { constraintErrors } from './constraint-errors';

interface ErrorResponse {
  message: string;
  status: number;
}

enum MySqlErrorCode {
  ER_DUP_ENTRY = 'ER_DUP_ENTRY',
}

@Catch()
export class AllExceptionsFilter implements ExceptionFilter {
  constructor(private readonly logger: Logger) {}

  catch(exception: any, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();

    let errorResponse: ErrorResponse;
    const message = exception?.response?.message || exception.message;

    if (exception instanceof HttpException) {
      errorResponse = { status: exception.getStatus(), message };
    } else if (exception instanceof QueryFailedError) {
      errorResponse = this.handleQueryExceptions(exception);
    } else {
      errorResponse = { message, status: HttpStatus.INTERNAL_SERVER_ERROR };
    }

    if (errorResponse.status === HttpStatus.INTERNAL_SERVER_ERROR) {
      this.logger.error(exception);
    }

    response.status(errorResponse.status).json({
      statusCode: errorResponse.status,
      timestamp: new Date().toISOString(),
      path: request.url,
      message: errorResponse.message,
    });
  }

  private handleQueryExceptions(exception: any): ErrorResponse {
    const status = HttpStatus.UNPROCESSABLE_ENTITY;
    const code = (exception as any).code;
    let message: string = (exception as QueryFailedError).message;

    switch (code) {
      case MySqlErrorCode.ER_DUP_ENTRY:
        message = constraintErrors[exception.constraint] || 'Already exists!';
        break;

      // case MySqlErrorCode.NotNullViolation: {
      //   const column = (exception as QueryFailedError).driverError.column;
      //   message = `${column.replace(/_/g, ' ')} is empty`;
      //   break;
      // }
    }

    return {
      message,
      status,
    };
  }
}
