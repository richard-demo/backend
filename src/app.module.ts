import { Module, RequestMethod } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { v4 as uuidv4 } from 'uuid';

import { UserModule } from 'modules/user/user.module';
import { AuthModule } from 'modules/auth/auth.module';
import { HealthCheckerModule } from 'modules/health_checker/health_checker.module';

import { SharedModule } from 'shared/shared.module';
import { ApiConfigService } from 'shared/services/api-config.service';
import { LoggerModule } from 'nestjs-pino';
import { RequestContextModule } from 'shared/request_context/request-context.module';

@Module({
  imports: [
    RequestContextModule,
    HealthCheckerModule,
    UserModule,
    AuthModule,
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: [`.env.${process.env.NODE_ENV}`, '.env'],
    }),
    TypeOrmModule.forRootAsync({
      imports: [SharedModule],
      useFactory: (configService: ApiConfigService) =>
        configService.mysqlConfig,
      inject: [ApiConfigService],
    }),
    LoggerModule.forRootAsync({
      imports: [SharedModule],
      inject: [ApiConfigService],
      useFactory: async (configService: ApiConfigService) => {
        return {
          pinoHttp: {
            level: (() => {
              const logLevel = {
                production: 'info',
                development: 'debug',
                test: 'error',
              };

              return logLevel[configService.nodeEnv] || 'info';
            })(),
            customLogLevel(req, res, err) {
              if (res.statusCode >= 400 && res.statusCode < 500) {
                return 'warn';
              } else if (res.statusCode >= 500 || err) {
                return 'error';
              } else if (res.statusCode >= 300 && res.statusCode < 400) {
                return 'silent';
              }
              return 'info';
            },
            // Define a custom request id function
            genReqId: function (req, res) {
              let id =
                req.headers['X-Request-Id'] || req.headers['x-request-id'];
              if (!id) id = uuidv4();

              res.setHeader('X-Request-Id', id);
              return id;
            },

            transport: configService.isDevelopment
              ? {
                  target: 'pino-pretty',
                  options: {
                    colorize: true,
                    levelFirst: true,
                    translateTime: 'UTC:mm/dd/yyyy, h:MM:ss TT Z',
                  },
                }
              : undefined,
            autoLogging: true,
            redact: ['req.headers.authorization'],
          },
          exclude: [{ method: RequestMethod.ALL, path: 'health/check' }],
        };
      },
    }),
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
