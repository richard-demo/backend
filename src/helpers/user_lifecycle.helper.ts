import { USER_STATUS } from 'entities/user.entity';

export function getUserErrorMessages(status: any) {
  switch (status) {
    case USER_STATUS.ARCHIVED:
      return 'The user has been archived, please contact the administrator to activate the account';
    default:
      return 'The user is not active';
  }
}
