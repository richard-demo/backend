export function lowercaseString(value: string) {
  return value?.toLowerCase()?.trim();
}
