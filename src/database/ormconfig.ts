import dotenv from 'dotenv';
import { DataSource } from 'typeorm';

import { SnakeNamingStrategy } from 'shared/services/snake-naming.strategy';
dotenv.config();
export const dataSource = new DataSource({
  type: 'mysql',
  host: process.env.DB_HOST,
  port: Number(process.env.DB_PORT),
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
  namingStrategy: new SnakeNamingStrategy(),
  subscribers: [],
  entities: [
    'src/entities/*.entity{.ts,.js}',
    'src/entities/*.view-entity{.ts,.js}',
  ],

  migrations: ['src/database/migrations/*{.ts,.js}'],
});
