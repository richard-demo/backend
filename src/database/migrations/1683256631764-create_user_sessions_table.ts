import { MigrationInterface, QueryRunner } from 'typeorm';

export class CreateUserSessionsTable1683256631764
  implements MigrationInterface
{
  name = 'CreateUserSessionsTable1683256631764';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            CREATE TABLE \`user_sessions\` (
                \`id\` varchar(36) NOT NULL,
                \`user_id\` varchar(255) NOT NULL,
                \`device\` varchar(255) NOT NULL,
                \`ip\` varchar(255) NOT NULL,
                \`created_at\` datetime NOT NULL,
                \`expiry\` datetime NOT NULL,
                \`revoked\` tinyint NOT NULL DEFAULT 0,
                PRIMARY KEY (\`id\`)
            ) ENGINE = InnoDB
        `);
    await queryRunner.query(`
            ALTER TABLE \`user_sessions\`
            ADD CONSTRAINT \`FK_e9658e959c490b0a634dfc54783\` FOREIGN KEY (\`user_id\`) REFERENCES \`users\`(\`id\`) ON DELETE CASCADE ON UPDATE NO ACTION
        `);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`
            ALTER TABLE \`user_sessions\` DROP FOREIGN KEY \`FK_e9658e959c490b0a634dfc54783\`
        `);
    await queryRunner.query(`
            DROP TABLE \`user_sessions\`
        `);
  }
}
